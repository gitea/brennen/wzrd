import analogio
import blinkenring
import board
import dotstar
import neopixel
import time
import urandom

random_pin = analogio.AnalogIn(board.A1)
urandom.seed(random_pin.value)
stick = blinkenring.BlinkenRing(board.D0, 8, 4)
stick.increment = -1
while True:
    # stick.party_mode = True
    stick.animate()
    time.sleep(0.2)
