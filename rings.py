from wzrd_util import *

# Accelerometer stuff not used at the moment:
# import adafruit_lis3dh
# import digitalio

import adafruit_thermistor
import analogio
import blinkenring
import board
import busio
import neopixel
import time
import urandom

# Hardware SPI setup for accelerometer, maybe (does not work):
# spi = busio.SPI(board.SCK, board.MOSI, board.MISO)
# cs = digitalio.DigitalInOut(board.D8)  # Set to appropriate CS pin!
# lis3dh = adafruit_lis3dh.LIS3DH_SPI(spi, cs)

# https://github.com/adafruit/Adafruit_CircuitPython_Thermistor
thermistor = adafruit_thermistor.Thermistor(board.TEMPERATURE, 10000, 10000, 25, 3950)

random_pin = analogio.AnalogIn(board.A4)
photocell = analogio.AnalogIn(board.A8)

# Initial setup:
tick_duration = 0.1
last_photocell = photocell_value(photocell)
frame = 0
max_frame = 30
max_bright = 0.35

ext_ring = blinkenring.BlinkenRing(board.A3, 24, 6)
onboard_ring = blinkenring.BlinkenRing(board.NEOPIXEL, 10, 5)
# ext_ring.party_mode = True
# onboard_ring.party_mode = True

while True:
    frame += 1
    ext_ring.animate()
    onboard_ring.animate()

    if frame == int(max_frame / 2):
        ext_ring.set_color(get_color_for_temp(thermistor.temperature))

    # reset frame counter
    if frame > max_frame:
        urandom.seed(random_pin.value)
        onboard_ring.set_color(ext_ring.get_color())
        ext_ring.randomize_color()
        frame = 0

    time.sleep(tick_duration)
