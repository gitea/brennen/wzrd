target = /media/brennen/CIRCUITPY

# wizard staff rings
ring:
	cp rings.py $(target)/main.py
	cp wzrd_util.py $(target)/
	cp blinkenring.py $(target)
#if [ -d $(target)/lib ]; then rm -r $(target)/lib; fi
#cp -r lib $(target)
	sync

# little 8-pixel stick
stick:
	cp stick.py $(target)/main.py
	cp blinkenring.py $(target)
	if [ -d $(target)/lib ]; then rm -r $(target)/lib; fi
	cp -r gemma_lib $(target)/lib
	sync
